# js-introduction

## Getting started + Mini exo Variable ([fichier js](./js/variable.js))
1. Cloner le projet, et dedans créer un fichier variable.html avec la structure html de base
2. Créer un dossier js et dedans créer un fichier variable.js
3. Faire un lien entre le html et le js avec une balise script avec un src dans le html
4. Dans le fichier variable.js, créer une variable que vous appelerez firstNumber et dedans mettre un nombre, celui que vous voulez
5. Afficher la variable dans la console de votre navigateur avec la commande console.log (je vous laisse regarder comment on l'utilise) et regarder le résultat en faisant F12 et onglet console
6. Faire ensuite une variable firstName avec votre prénom en chaîne de caractère dedans et l'afficher en console aussi et faire une variable age avec votre age dedans

## Exo conditions ([fichier js](./js/condition.js))
1. Créer deux nouveaux fichier condition.html et condition.js et les linker toussa toussa
2. Dans le fichier js, créer une variable age et mettez un nombre dedans
3. En dessous, faire une condition qui affichera dans la console "Cheers" si l'age est supérieur ou égal à 18
4. Sinon, sera afficher dans la console "Here's some juice"
5. Créer une variable isBirthday avec un booléen à l'intérieur
6. Avant l'autre if, faire un nouveau if qui vérifie si isBirthday est true, et si oui, ajoutera 1 à la variable age et affichera "Happy Birthday"
7. Créer une nouvelle variable myName dans laquelle vous mettrez votre nom de famille
8. Faire une condition qui vérifie si myName vaut "Demel", si oui afficher "Teacher is You", si non afficher "Student is You"
9. Mais vu que c'est pas toujours Demel le formateur et que parfois c'est Brogard, trouver un moyen de pouvoir modifier facilement le nom du formateur sans faire d'avantage de condition
10. Bonus : Dans la deuxième condition, faire que ça écrive "Happy Birthday " suivi du nom que vous avez mis dans la variable myName

## Conditions && || ([fichier js](./js/condition.js))
1. Si le teacher est "Brogard" et que c'est mon anniversaire, afficher "Pierre brings a cake"
2. Si mon age est entre 30 et 39 ans, afficher "my best years (or not)"
3. Si ce n'est pas mon anniversaire ou que j'ai moins de 17 ans, afficher "I won't vote this year (oh no so sad )"
4. Créer une variable year avec l'année actuelle dedans puis faire en sorte d'afficher "big party for my thirties in ..." suivi de l'année de vos 30 ans (calculé à partir de l'age et l'année actuelle), seulement si l'age est inférieur à 30


## Loop Intro ([fichier js](./js/loop.js))
1. Créer deux nouveaux fichier loop.html et .js
2. Dans le fichier js, créer une variable count et l'initialiser à 0
3. Faire ensuite un while (qui a exactement la même syntaxe que le if) qui fera un console log de count tant que count est inférieur à 10
Faire en sorte qu'à l'intérieur du while, la valeur de count augmente de 1

## Exercice Deviner un nombre ([fichier js](./js/loop.js))
1. Créer une variable numberToGuess dans laquelle vous mettrez le nombre que vous voulez faire deviner
2. Faire une variable success initialisée à false
3. Faire une boucle while qui s'arrêtera si success est true
4. A l'intérieur de cette boucle, faire un prompt pour demander à l'utilisateur.ice un nombre et stocker la réponse dans une variable
5. Toujours dans la boucle, faire des if qui vont faire que si le nombre est inférieur à numberToGuess ça affiche "it's more", s'il est suéprieur ça affiche "it's less", si c'est le bon, ça passe la variable success à true
6. Bonus: faire en sorte que le nombre à deviner soit également demander par un prompt avant de commencer le jeu