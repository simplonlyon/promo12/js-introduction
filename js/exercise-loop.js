let lineNumber = 10;
let starNumber = 1;
let spaceNumber = lineNumber-1;

for(let line = 0; line < lineNumber; line++) {
    let toLog = "";

    // toLog += " ".repeat(spaceNumber);
    for(let col = 0; col < spaceNumber; col++) {
        toLog += " ";
    }
    // toLog += "*".repeat(starNumber);
    for(let col = 0; col < starNumber; col++) {
        toLog += "*";
    }

    console.log(toLog);
    starNumber += 2;
    spaceNumber--;
}
