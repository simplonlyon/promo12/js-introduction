/* Pour tous les codes suivants (séparés par des commentaires), identifier :
 - l'ordre d'exécution des instructions
 - Les variables uniques (quelle variable est la même variable qu'une autre)
 - Les valeurs contenues dans les variables au fur et à mesure éventuellement
*/

// Code 1

let maVariable = 'bloup';
maVariable = 'test';

console.log(maVariable);

maVariable = 'autre';


// Code 2

let variable = 'bloup';

if(variable.length > 10) {
    variable += 'blip';
}

console.log(variable);

// Code 3

let uneVariable = 10;
let autreVariable = maFonction();

console.log(uneVariable);
console.log(autreVariable);

function maFonction() {
    let uneVariable = 20;
    uneVariable += 5;
    return uneVariable;
}

// Code 4

let numA = 10;
let numB = 5;
let result = divide(numA, numB);
console.log(result);

numA = 15;
numB = 0;
result = divide(numA, numB);
console.log(result);

result = divide(numB, numA);
console.log(result);


function divide(a, b) {
    if(b !== 0) {
        return a/b;
    }
    return null;
}





// Code 5

let maPresentation = presentationString('Dupont', 'Paul', 40);
console.log(maPresentation);

let personne = {
    nom: 'Smith',
    prenom: 'John',
    age: 30
}
let myPres = presentationString(personne.nom, personne.prenom, personne.age);
function presentationString(nom, prenom, age) {
    return `Bonjour, je m'appelle ${prenom} ${nom}, j'ai ${age} ans`;
}


// Code 6

let nouveau = 'bloup';
let tableau = ['ga', 'zo', 'bu', 'meu'];
let trouve = rechercheTableau(tableau, nouveau);

if(!trouve) {
    tableau.push(noveau);
}
console.log(tableau)

function rechercheTableau(tableau, recherche) {
    for(let item of tableau) {
        if(item === recherche) {
            return true;
        }
    }
    return false;
}