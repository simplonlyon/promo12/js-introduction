let age = 29;

let isBirthday = false;

let myName = "Sophie";

let teacher = "Demel";

let year = 2019;
//On vérifie si le contenu de la variable myName correspond strictement
//au contenu de la variable teacher
if (myName === teacher) {
    console.log("Teacher is You");
} else {
    console.log("Student is You");
}

//On vérifie si la variable isBirthday est true
// if(isBirthday === true) {
if (isBirthday) {
    age++; //équivaut à...
    // age += 1; // ...qui équivaut à...
    // age = age+1;
    //On concatène le contenu de la variable myName avec la chaîne de
    //caractère "Happy Birthday ", on peut le faire soit avec un +
    console.log("Happy Birthday " + myName);
    //Soit avec les back ticks et le ${variable}
    console.log(`Happy Birthday ${myName}`);
}
//On vérifie si la valeur de la variable age est supérieure ou égale à 18
if (age >= 18) {
    console.log("Cheers !");

} else {
    console.log("Have some juice !");
}

if (teacher === "Brogard" && isBirthday) {
    console.log("Pierre brings a cake");
}

if( age >= 30 && age <= 39 ) {
    console.log("My best years ?");
}

if(!isBirthday || age < 17) {
    console.log("I won't vote this year");
}

if(age < 30) {
    let thirtiesYear = year + 30 - age;
    console.log("Big party for my thirties in "+thirtiesYear);
    
}

